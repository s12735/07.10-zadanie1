import java.util.Date;

public class Advert {
	Car car;
	String region;
	String city;
	Date date;
	
	public Advert(){};
	
	public Advert(Car car, String region, String city, Date date) {
		this.car = car;
		this.region = region;
		this.city = city;
		this.date = date;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Car=" + car + ", region=" + region + ", city=" + city + ", date=" + date + "]";
	}
	
	// method to sort by year
	public int compareTo(Car o) {
        int compareYear = car.year.compareTo(o.year);
 
        if(compareYear == 0) {
            return car.year.compareTo(o.year);
        }
        else {
            return compareYear;
        }
    }
}
