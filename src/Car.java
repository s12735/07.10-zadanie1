public class Car implements iCar {
	String brand;
	Integer price;
	Integer year;
	long przebieg;
	boolean newCar, damaged;
	

	public Car(){};
	
	public Car(String brand, int price, int year, long przebieg, boolean newCar, boolean damaged) {
		this.brand = brand;
		this.price = price;
		this.year = year;
		this.przebieg = przebieg;
		this.newCar = newCar;
		this.damaged = damaged;
	}

	@Override
	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public void setMileage(long przebieg) {
		this.przebieg = przebieg;
	}

	@Override
	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public void setCarDamaged(boolean check) {
		this.damaged = check;
	}

	@Override
	public void setCarNew(boolean check) {
		this.newCar = check;
	}

	@Override
	public String getBrand() {
		return brand;
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public long getMileage() {
		return przebieg;
	}

	@Override
	public int getYear() {
		return year;
	}

	@Override
	public boolean getCarDamaged() {
		return damaged;
	}

	@Override
	public boolean getCarNew() {
		return newCar;
	}

	@Override
	public String toString() {
		return "[Brand=" + brand + ", price=" + price + ", year=" + year + ", przebieg=" + przebieg + ", newCar="
				+ newCar + ", damaged=" + damaged + "]";
	}
	
	// method to sort by year
	public int compareTo(Car o) {
        int compareYear = year.compareTo(o.year);
 
        if(compareYear == 0) {
            return year.compareTo(o.year);
        }
        else {
            return compareYear;
        }
    }

}
