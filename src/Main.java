import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Main {
	public static void main(String[] args) {

		List<Advert> advertList = new ArrayList<>();
		List<Car> carList = new ArrayList<>();

		carList.add(new Car("Ford", 12500, 2013, 48240, false, false));
		carList.add(new Car("Skoda", 9850, 2001, 182050, false, false));

		advertList.add(new Advert(carList.get(0), "Pomorze", "Puck", new Date()));
		advertList.add(new Advert(carList.get(1), "Pomorze", "Gda�sk", new Date()));

		// wy�wietlenie wszystkich aut z og�osze�
		System.out.println("All advert in base:");
		for (int i = 0; i < advertList.size(); i++) {
			System.out.println(advertList.get(i));
		}

		// wy�wietlenie wszystkich aut z miasta Gda�sk
		System.out.println("\nCar from Gdansk:");
		for (int i = 0; i < advertList.size(); i++) {
			if (advertList.get(i).getCity() == "Gda�sk") {
				System.out.println(advertList.get(i));
			}
		}

		// Advert order By year
		Collections.sort(advertList, new Comparator<Advert>() {
			public int compare(Advert c1, Advert c2){
				int year =  c1.getCar().getYear() - c2.getCar().getYear();
	            if(year == 0) {
	            	return c1.getCar().compareTo(c2.getCar());
	            }
	            return year;
			}
		});
		
		System.out.println("\nSorted by Year:");
		for (Advert adv : advertList) {
			System.out.println(adv);
		}
	}
	
}