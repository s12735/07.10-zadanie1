public interface iCar {
	void setBrand(String brand);
	void setPrice(int price); 
	void setMileage(long przebieg);
	void setYear(int rocznik);
	void setCarDamaged(boolean check);
	void setCarNew(boolean check);
	
	String getBrand();
	int getPrice();
	long getMileage();
	int getYear();
	boolean getCarDamaged();
	boolean getCarNew();
}
